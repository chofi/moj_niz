#include<iostream>
#include<initializer_list>
#include<stdexcept>

using namespace std;

class MojNizInt{
  private:
    int* arr_ = nullptr;
    int size_ = 0;
    int cap_ = 1;
    
    void reallocate(){ 
      cap_ *= 2;
      int* temp = new int[cap_];
      copy(arr_, arr_ + size_, temp);
      delete [] arr_;
      arr_ = temp;
    }

  public:
    MojNizInt() = default;
    MojNizInt(initializer_list<int> a) : size_(a.size()), cap_(a.size()), arr_(new int[a.size()]) {
      move(begin(a), end(a), arr_);
    }
    ~MojNizInt(){ delete [] arr_; }
    MojNizInt(const MojNizInt& src) : cap_(src.cap_), size_(src.size()), arr_(new int[src.cap_]){
      copy(src.arr_, src.arr_ + src.size_, arr_);
    }
    MojNizInt(MojNizInt&& src) : cap_(src.cap_), size_(src.size()), arr_(new int[src.cap_]){
      move(src.arr_, src.arr_ + src.size_, arr_);
      src.cap_ = 0;
      src.size_ = 0;
      src.arr_ = nullptr;
    }

    MojNizInt& operator=(const MojNizInt& src){
      cap_ = src.cap_;
      size_ = src.size_;
      delete [] arr_;
      arr_ = new int[cap_];
      copy(src.arr_, src.arr_ + size_, arr_);

      return *this;
    }
    MojNizInt& operator=(MojNizInt&& src){
      if(this != &src){
        delete [] arr_;
        arr_ = src.arr_;
        size_ = src.size_;
        cap_ = src.cap_;
        src.cap_ = 0;
        src.size_ = 0;
        src.arr_ = nullptr;
      }
      return *this;
    }
    int& operator[](int i) const{
      if(i < this->size_)
        return this->arr_[i];
      else
        throw std::out_of_range("Index out of boundaries.");
    }
    MojNizInt operator*(const int& scalar) const{
        MojNizInt newAr = *this;
        for(int i = 0; i < size_; ++i)
          newAr[i] *= scalar;
        return newAr;
    }
    MojNizInt operator+(const MojNizInt& scnd) const{
      if(size_ != scnd.size_)
        throw std::invalid_argument("Index out of boundaries.");
      else{
        MojNizInt newAr(*this);
        for(int i = 0; i < size_; ++i)
          newAr[i] += scnd[i];
        return newAr;
      }
    }
    MojNizInt& operator++(){
      for(int i = 0; i < this->size_; ++i)
        ++this->arr_[i];
      return *this;
    }
    MojNizInt operator++(int){
      MojNizInt toReturn(*this);
      for(int i = 0; i < this->size_; ++i)
        ++this->arr_[i];
      return toReturn;
    }

    int size() const{
      return size_;
    }

    int& at(int i) const{
      if(i <= size_ && size_ > 0 && i >= 0)
        return arr_[i];
      else 
        throw std::out_of_range("Index out of boundaries.");
    }

    int capacity(){ return cap_; }

    void pop_back(){ --size_; }

    int front(){ return this->arr_[0]; }

    int back(){ return this->arr_[size_-1]; }

    void push_back(int num){
      if(size_ == 0)
        arr_ = new int[cap_];
      if(size_ >= cap_) reallocate();

      ++size_;
      arr_[size_ - 1] = num;
    }
};

